package com.java1.cards;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * 
 * 
 * @author Gyasi Story
 * @version 1.0
 * 
 */
public class MainActivity extends Activity {

	public static char get;
	static EditText edit;

	/**
	 * Human player
	 */
	final static int HUMAN = 0;
	/**
	 * Computer Player
	 */
	final static int COMPUTER = 1;
	/**
	 * Message variable
	 */
	public static String msg;
	public static Deck deck = new Deck(); // Deck automaticall shuffled
	public static DiscardPile discardPile = new DiscardPile();
	public static Card hCard;
	public static Card cCard;
	public static int choice;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		/**
		 * Set UI Variables
		 */
		TextView label = (TextView) findViewById(R.id.label);
		label.setText("Welcome to Four of a Kind!");
		Button button = (Button) findViewById(R.id.button1);
		edit = (EditText) findViewById(R.id.editText1);
		TextView display = (TextView) findViewById(R.id.cardDisplay);
		
		//while (true) {
			hCard = deck.deal();
			cCard = deck.deal();
			if (hCard.rank() != cCard.rank())
				// break;
				deck.putBack(hCard);
			deck.putBack(cCard);
			deck.shuffle(); // prevent pathological case where every successive
		//} // end While
		
		int curPlayer = HUMAN;
		if (cCard.rank().ordinal() > hCard.rank().ordinal())
			curPlayer = COMPUTER;
		deck.putBack(hCard);
		hCard = null;
		deck.putBack(cCard);
		cCard = null;
		Card[] hCards = new Card[4];
		Card[] cCards = new Card[4];
		if (curPlayer == HUMAN)
			for (int i = 0; i < 4; i++) {
				cCards[i] = deck.deal();
				hCards[i] = deck.deal();
			}
		else
			for (int i = 0; i < 4; i++) {
				cCards[i] = deck.deal();
				hCards[i] = deck.deal();
			}

		//while (true) {
			if (curPlayer == HUMAN) {
				FourOfAKind.showHeldCards(hCards);
				while (choice < '1' || choice > '4') {
					switch (choice) {
					case 'a':
						choice = '1';
						break;
					case 'b':
						choice = '2';
						break;
					case 'c':
						choice = '2';
						break;
					case 'd':
						choice = '4';
						break;
					}
				}
				discardPile.SetTopCard(hCards[choice - 'A']);
				hCards[choice - 'A'] = deck.deal();
				if (FourOfAKind.isFourOfAKind(hCards)) {
					msg = "\n" + "Human Wins" + "\n";
					FourOfAKind.putDown(hCards);
					msg += "\n";
					FourOfAKind.putDown(cCards);
					return; // Exit application by returning from main()
				}
				curPlayer = COMPUTER;
			} else {
				choice = FourOfAKind.leastDesirableCard(cCards);
				discardPile.SetTopCard(cCards[choice]);
				cCards[choice] = deck.deal();
				if (FourOfAKind.isFourOfAKind(cCards)) {
					msg = "\n" + "Computer Wins!" + "\n";
					FourOfAKind.putDown(cCards);
					return; // Exit application by returning from main()
				}
				curPlayer = HUMAN;
			}
			if (deck.isEmpty()) {
				while (discardPile.topCard() != null)
					deck.putBack(discardPile.getTopCard());
				deck.shuffle();
			}
		//}
		display.setText(msg);

		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				choice = Integer
						.parseInt(edit.getText().toString());
				System.out.println(choice);

			}

		});

		// FourOfAKind.running();

	}

}
