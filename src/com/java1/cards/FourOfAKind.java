package com.java1.cards;







/**
 * <code>FourofAKind</code>implements a card game that is played between two
 * players: one human player and the computer.  YOu play this game with a
 * standard 52-card deck and attempt to beat the computer by being the first
 * player to put down four cards that have the same rank (four aces, for
 * example), and win.
 * 
 * <p>
 * The game begins by shuffling the deck and placing it face down. Each
 * player takes a card from the top of the deck.  The player with the highest 
 * ranked card (king is highest) deals four cards to each player starting 
 * with the other player.  The dealer then starts is turn.
 * 
 *  <p>
 *  The player examine its card to determine which cards are optimal for
 *  achieving four of a kind.  The player then throws away one card on  a 
 *  discard pile and picks up another card form the top of the deck. If the 
 *  player has four of a kind, the player puts down these cards (face up) and
 *  wins the game.
 *   
 * @author Gyasi Story
 * @version 1.0
 *
 */
public class FourOfAKind 
{
	
	
	/**
	 * running game
	 */
	static void running(){
		
	}	//end Running Game
	
	/**
	 * Determine if the <code>Card</code> objects passed to this method all
	 * have the same rank.
	 * 
	 * @param cards array of <code>Card</code> objects passed to this method
	 * 
	 * @return true if all <code> Card</code> objects have the same rank;
	 * otherwise, false
	 */
	static boolean isFourOfAKind(Card[] cards){
		for(int i =1; i<cards.length; i++)
			if (cards[i].rank() != cards[0].rank())
				return false;
		return true;
	}
	/**
	 * Identify one of the <code>Card</code> objects that is passed to this
	 * methods as the least desireable <code>Card</code> object to hold onto
	 * 
	 * @param cards array of <code>Card</code> objects passed to this method
	 * 
	 * @return 0-based rank (ace is 0, king is 13) of least desirable card
	 */
	static int leastDesirableCard(Card[] cards){
		int[] rankCounts = new int[13];
		for (int i=0; i<cards.length; i++)
			rankCounts[cards[i].rank().ordinal()]++;
		int minCount = Integer.MAX_VALUE;
		int minIndex = -1;
		for(int i=0; i<rankCounts.length; i++)
			if (rankCounts[i] <minCount && rankCounts[i] != 0){
				minCount = rankCounts[i];
				minIndex = i;
			}
		for (int i = 0; i<cards.length; i++)
			if(cards[i].rank().ordinal() == minIndex)
				return i;
		return 0;	//Needed to satify compiler (should never be executed)
	}
	/**
	 * display a message followed by all cards held by player. this output
	 * simulates putting down held cards.
	 * 
	 * @param msg message to be displayed to human player
	 * @param cards array of <code>Card</code> objects to be identified
	 */
	static void putDown(Card[] cards){
		for (int i = 0; i <cards.length; i++)
			MainActivity.msg+= cards[i] + "\n";
	}
	/**
	 * Identify the cards being held big their <code>Card</code> objects on
	 * separate lines.  Prefix each line with an uppercase letter starting with
	 * <code>A</code>.
	 * 
	 * @param cards array of <code>Card</code> objects to be identified
	 */
	static void showHeldCards (Card[] cards){
		MainActivity.msg += "Held cards: " + "\n";
		for(int i = 0; i < cards.length; i++)
			MainActivity.msg += (char)('A'+i) + ". " + cards[i]+ "\n";
		MainActivity.msg += "\n";
	}

}












