package com.java1.cards;
/**
 * 
 * Simulate a pile of discarded cards
 * 
 * @author Gyasi Story
 *
 */
public class DiscardPile {
	private Card[] cards;
	private int top;
	/**
	 * Create a <code>DiscardPile</code> that can accomadate a maximum of 52
	 * <code>Card</code>s.  The <code> DiscardPile</code> is intialy empty.
	 */
	public DiscardPile() {
		cards = new Card[52];  	//Room for entire deck on dicard pile (should
								//never happen).
		top = -1;
				
	}
	/**
	 * Return the <code>Card</code> at the top of the <code>DiscardPile</code>.
	 * 
	 * @return <code>Card</code> object at top of <code>DiscardPile</code> or
	 * null if <code>DiscardPile</code> is empty
	 */
	public Card getTopCard() {
		if (top == -1)
			return null;
		Card card = cards[top];
		cards[top--] = null;
		return card;
	}
	/**
	 * Set the <code> DiscardPile</code>'s top card to the specified
	 * <code>Card</code> object.
	 * 
	 * @param card <code>Card</code> object being thrown on top of the 
	 * <code>DiscardPile</code>
	 */
	public void SetTopCard(Card card)
	{
		cards[++top] = card;
	}
	/**
	 * Identify the top <code> Card</code on the <code>DiscardPiled</code>
	 * without removing this <code>Card</code>
	 * 
	 * @return top<code>Card</code>, or null if <code>DiscardPile</code> is
	 * empty
	 */
	public Card topCard(){
		return (top == -1) ? null :cards[top];
	}
	

}
